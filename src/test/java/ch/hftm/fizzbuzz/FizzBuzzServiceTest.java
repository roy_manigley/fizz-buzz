package ch.hftm.fizzbuzz;

import fizzbuzz.FizzBuzzService;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class FizzBuzzServiceTest {

    @Test
    public void pruefeNormaleZahl() {
        String result;
        result = FizzBuzzService.getResultOfNumber(2);
        assertEquals("2", result,  "Normale Zahl als Resultat erwartet");

        result = FizzBuzzService.getResultOfNumber(103);
        assertEquals("103", result,  "Normale Zahl als Resultat erwartet");
    }

    @Test
    public void pruefeDurchDreiTeilbareZahl() {
        String result;
        result = FizzBuzzService.getResultOfNumber(3);
        assertEquals("Fizz", result , "Fizz als Resultat erwartet");

        result = FizzBuzzService.getResultOfNumber(99);
        assertEquals("Fizz", result , "Fizz als Resultat erwartet");
    }


    @Test
    public void pruefeDurchFuenfTeilbareZahl() {
        String result;
        result = FizzBuzzService.getResultOfNumber(5);
        assertEquals("Buzz", result, "Buzz als Resultat erwartet");

        result = FizzBuzzService.getResultOfNumber(100);
        assertEquals("Buzz", result, "Buzz als Resultat erwartet");
    }


    @Test
    public void pruefeDurchFuenfUndDreiTeilbareZahl() {
        String result;
        result = FizzBuzzService.getResultOfNumber(15);
        assertEquals("FizzBuzz", result, "FizzBuzz als Resultat erwartet");

        result = FizzBuzzService.getResultOfNumber(75);
        assertEquals("FizzBuzz", result, "FizzBuzz als Resultat erwartet");

    }
}
