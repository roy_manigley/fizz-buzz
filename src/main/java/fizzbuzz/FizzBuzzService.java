package fizzbuzz;

public class FizzBuzzService {
    public static String getResultOfNumber(int i) {
        String returnValue = "";
        if (i % 3 == 0)
            returnValue += "Fizz";
        if (i % 5 == 0)
            returnValue += "Buzz";
        return returnValue.isEmpty() ? Integer.toString(i) : returnValue;
    }
}
